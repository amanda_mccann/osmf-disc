# Diversity

## Diversity Statement

The OpenStreetMap Foundation and the global OpenStreetMap community welcome and encourage participation by everyone. Our community is based on mutual respect, tolerance, and encouragement, and we are working to help each other live up to these principles. We want our community to be more diverse: whoever you are, and whatever your background, we welcome you.

## Diversity Appendix

We have created this diversity statement because we believe that a diverse OpenStreetMap community is stronger and more vibrant. A diverse community where people treat each other with respect has more potential contributors and more sources for ideas, and will create a better map.

Although we have phrased the formal diversity statement generically to make it all-inclusive, we recognize that there are specific attributes that are used to discriminate against people. In alphabetical order, some of these attributes include (but are not limited to): age, body size, culture, disability, ethnicity, gender, gender expression, language(s) understood, level of formal education, national origin, nationality, neuro(a)typicality, physical appearance, physical or mental difference(s) or illness(s), race, religion or belief systems, or lack of, sex, sexual orientation, socio-economic status or subculture. We welcome people regardless of the values of these or other attributes.

The OpenStreetMap Foundation and community welcomes people no matter what languages they can speak or understand. Historically, and at present, core OpenStreetMap work is nearly entirely conducted in English. The OpenStreetMap Foundation encourages the creation of local chapters everywhere, and many of them are listed on our wiki. Many of these chapters also have mailing lists, or other communication channels, in the locally preferred language.

The writers of these guidelines are deeply indebted to the [Python Software Foundation](https://www.python.org/community/diversity/).
